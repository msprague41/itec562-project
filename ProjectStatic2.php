<!DOCTYPE html>
<html>

<title>ITEC562-001 Project</title>

<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">

<style>
    body,h1,h2,h3,h4,h5,h6 {font-family: "Lato", sans-serif}

    .w3-navbar,h1,button {font-family: "Montserrat", sans-serif}
    .fa-database {font-size:200px}
</style>

<body>
<!-- Navbar -->
<div class="w3-top">
  <ul class="w3-navbar w3-red w3-card-2 w3-left-align w3-large">
    <li class="w3-hide-medium w3-hide-large w3-opennav w3-right">
      <a class="w3-padding-large w3-hover-white w3-large w3-red" href="javascript:void(0);" onclick="myFunction()" title="Toggle Navigation Menu"><i class="fa fa-bars"></i></a>
    </li>
    
    <li><a href="ProjectIndex.php" class="w3-padding-large w3-white">Home</a></li>
    
    <li class="w3-hide-small"><a href="ProjectContact.php" class="w3-padding-large w3-hover-white">Create Account</a></li>
    
    <li class="w3-dropdown-hover">
		<a>Static Queries <i class="fa fa-caret-down"></i></a>
			<div class="w3-dropdown-content w3-white w3-card-4">
				<a href="ProjectStatic1.php">Query 1</a>
				<a href="ProjectStatic2.php">Query 2</a>
				<a href="ProjectMultipleTable.php">Multiple Table Query</a>
			</div>
	</li>
    
    <li class="w3-dropdown-hover">
		<a>Ad Hoc Queries <i class="fa fa-caret-down"></i></a>
			<div class="w3-dropdown-content w3-white w3-card-4">
				<a href="ProjectAdHocSelectForm.php">Select Query</a>
				<a href="ProjectAdHocUpdateForm.php">Update Query</a>
			</div>
	</li>
    
    <li class="w3-hide-small"><a href="ProjectResources.php" class="w3-padding-large w3-hover-white">Resources Used</a></li>
	
  </ul>

<!-- Navbar on small screens -->
  <div id="navDemo" class="w3-hide w3-hide-large w3-hide-medium">
    <ul class="w3-navbar w3-left-align w3-large w3-black">
      
      <li><a class="w3-padding-large" href="ProjectContact.php">Create Account</a></li>
      
	  <li class="w3-dropdown-hover">
		<a>Static Queries <i class="fa fa-caret-down"></i></a>
			<div class="w3-dropdown-content w3-white w3-card-4">
				<a href="ProjectStatic1.php">Query 1</a>
				<a href="ProjectStatic2.php">Query 2</a>
				<a href="ProjectMultipleTable.php">Multiple Table Query</a>
			</div>
      </li>
      
      <li class="w3-dropdown-hover">
		<a>Ad Hoc Queries <i class="fa fa-caret-down"></i></a>
			<div class="w3-dropdown-content w3-white w3-card-4">
				<a href="ProjectAdHocSelectForm.php">Select Query</a>
				<a href="ProjectAdHocUpdateForm.php">Update Query</a>
			</div>
	  </li>
      
      <li><a class="w3-padding-large" href="ProjectResources.php">Resources Used</a></li>
    </ul>
  </div>
</div>

<!-- Header -->
<header class="w3-container w3-red w3-center w3-padding-128">
  <h1 class="w3-margin w3-jumbo">Static Query 2</h1>
  <p class="w3-xlarge">Showcases the ability to query the database for a result</p>
</header>

<!-- First Grid -->
<div class="w3-row-padding w3-padding-64 w3-container">
  <div class="w3-content">
    <div class="w3-twothird">
      <h1>Query Results</h1>

      <?php
            echo "<table style='border: solid 3px #f44336;'>";
            echo "<tr><th>First Name</th><th>Last Name</th><th>E-mail</th></tr>";

            class TableRows extends RecursiveIteratorIterator { 
                    function __construct($it) { 
                    parent::__construct($it, self::LEAVES_ONLY); 
                }

                    function current() {
                    return "<td style='width:150px;border:1px solid #f44336; text-align:center;'>" . parent::current(). "</td>";
                }

                    function beginChildren() { 
                    echo "<tr>"; 
                    } 

                    function endChildren() { 
                    echo "</tr>" . "\n";
                    } 
            } 


            $servername = "localhost";
            $username = "username";
            $password = "password";
            $dbname = "sakila";


            try {
                $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
                $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $stmt = $conn->prepare("SELECT first_name, last_name, email FROM customer WHERE active < 1");
                $stmt->execute();

                // set the resulting array to associative
                $result = $stmt->setFetchMode(PDO::FETCH_ASSOC); 
                foreach(new TableRows(new RecursiveArrayIterator($stmt->fetchAll())) as $k=>$v) { 
                    echo $v;
                }
            }
            
            catch(PDOException $e) {
                echo "Error: " . $e->getMessage();
            }

            $conn = null;

            echo "</table>";
        ?>
    </div>

    <div class="w3-third w3-center">
        <h1>About the Query</h1>
        <h5> 
            This query displays the first name, last name, and email of the customers
            with inactive accounts. This is a static select query utilizing records 
            from the customer table of the Sakila database. A user would find this query 
            helpful if they were looking for customers to contact about reactivating 
            their acount.
        </h5>
        <i class="fa fa-database w3-padding-64 w3-text-red w3-margin-right"></i>
    </div>   
  </div>
</div>

<!-- Footer -->
<footer class="w3-container w3-black w3-center w3-opacity w3-padding-8">
    <p>Powered by <a href="http://www.w3schools.com/w3css/default.asp" target="_blank">w3.css</a></p>
</footer>

<script>
    // Used to toggle the menu on small screens when clicking on the menu button
    function myFunction() {
        var x = document.getElementById("navDemo");
        if (x.className.indexOf("w3-show") == -1) {
            x.className += " w3-show";
        } else {
            x.className = x.className.replace(" w3-show", "");
        }
    }
</script>

</body>
</html>

