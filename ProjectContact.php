<!DOCTYPE html>
<html>

<title>ITEC562-001 Project</title>

<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">

<style>
    body,h1,h2,h3,h4,h5,h6 {font-family: "Lato", sans-serif}
    td{width:150px; border:1px solid #f44336; padding:5px; margin:5px;}
    
    .w3-navbar,h1,button {font-family: "Montserrat", sans-serif}
    .error {color: red;}
    .noerror {color: black}
    .fa-user-plus {font-size:200px}
</style>

<body>
<!-- Navbar -->
<div class="w3-top">
  <ul class="w3-navbar w3-red w3-card-2 w3-left-align w3-large">
    
    <li class="w3-hide-medium w3-hide-large w3-opennav w3-right">
      <a class="w3-padding-large w3-hover-white w3-large w3-red" href="javascript:void(0);" onclick="myFunction()" title="Toggle Navigation Menu"><i class="fa fa-bars"></i></a>
    </li>
    
    <li><a href="ProjectIndex.php" class="w3-padding-large w3-white">Home</a></li>
    
    <li class="w3-hide-small"><a href="ProjectContact.php" class="w3-padding-large w3-hover-white">Create Account</a></li>
    
    <li class="w3-dropdown-hover">
		<a>Static Queries <i class="fa fa-caret-down"></i></a>
			<div class="w3-dropdown-content w3-white w3-card-4">
				<a href="ProjectStatic1.php">Query 1</a>
				<a href="ProjectStatic2.php">Query 2</a>
				<a href="ProjectMultipleTable.php">Multiple Table Query</a>
			</div>
	</li>
    
    <li class="w3-dropdown-hover">
		<a>Ad Hoc Queries <i class="fa fa-caret-down"></i></a>
			<div class="w3-dropdown-content w3-white w3-card-4">
				<a href="ProjectAdHocSelectForm.php">Select Query</a>
				<a href="ProjectAdHocUpdateForm.php">Update Query</a>
			</div>
	</li>
    
    <li class="w3-hide-small"><a href="ProjectResources.php" class="w3-padding-large w3-hover-white">Resources Used</a></li>
	
  </ul>

<!-- Navbar on small screens -->
  <div id="navDemo" class="w3-hide w3-hide-large w3-hide-medium">
    <ul class="w3-navbar w3-left-align w3-large w3-black">
      
      <li><a class="w3-padding-large" href="ProjectContact.php">Create Account</a></li>
      
	  <li class="w3-dropdown-hover">
		<a>Static Queries <i class="fa fa-caret-down"></i></a>
			<div class="w3-dropdown-content w3-white w3-card-4">
				<a href="ProjectStatic1.php">Query 1</a>
				<a href="ProjectStatic2.php">Query 2</a>
				<a href="ProjectMultipleTable.php">Multiple Table Query</a>
			</div>
      </li>
      
      <li class="w3-dropdown-hover">
		<a>Ad Hoc Queries <i class="fa fa-caret-down"></i></a>
			<div class="w3-dropdown-content w3-white w3-card-4">
				<a href="ProjectAdHocSelectForm.php">Select Query</a>
				<a href="ProjectAdHocUpdateForm.php">Update Query</a>
			</div>
	  </li>
      
      <li><a class="w3-padding-large" href="ProjectResources.php">Resources Used</a></li>
    </ul>
  </div>
</div>

<!-- Header -->
<header class="w3-container w3-red w3-center w3-padding-128">
    <h1 class="w3-margin w3-jumbo">Create Account</h1>
    <p class="w3-xlarge">Enter your contact information and press 'Submit' to create an account within our database</p>
</header>

<!-- First Grid -->
<div class="w3-row-padding w3-padding-64 w3-container">
  <div class="w3-content">
    <div class="w3-twothird">
        <h1>Input Form</h1>
      
        <?php

            $fname = $lname = $phone = $email = $address = $sanitizePhone = "";
            $fnameErr = $lnameErr = $phoneErr = $emailErr = "";
            $nameErrFlag = $phoneErrFlag = $emailErrFlag = true;
            $errFlag = true;
            $servername = "localhost";
            $username = "username";
            $password = "password";
            $dbname = "sakila";


            if ($_SERVER["REQUEST_METHOD"] == "POST") {
  
                if (empty($_POST["fname"])) {
                    $fnameErr = "First name is required";
                } 
                else {
                    $fname = test_input($_POST["fname"]);
    
                    if (!preg_match("/^[a-zA-Z ]*$/",$fname)) {
                        $fnameErr = "Only letters and white space allowed in <b>First Name</b>";  
                    }
                    
                    $nameErrFlag = false;
                }
   
                if (empty($_POST["lname"])) {
                    $lnameErr = "Last name is required";
                } 
                else {
                    $lname = test_input($_POST["lname"]);
    
                    if (!preg_match("/^[a-zA-Z ]*$/",$lname)) {
                        $lnameErr = "Only letters and white space allowed in <b>Last Name</b>";
                    }
                    
                    $nameErrFlag = false;
                }

                if (empty($_POST["phone"])) {
                    $phoneErr = "Phone number is required";
                }
                else {
                    $phone = test_input($_POST["phone"]);
                    $sanitizePhone = preg_replace('#[^0-9]#', '', $phone);
  
                    if (!preg_match("/\d{10}/", $sanitizePhone)) {
                        $phoneErr = "Phone number must be in '(###) ###-####' format";
                    }
                    
                    $phoneErrFlag = false;
    
                }

                if (empty($_POST["email"])) {
                    $emailErr = "Email is required";
                } 
                else {
                    $email = test_input($_POST["email"]);
   
                    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                        $emailErr = "Email must be in 'example@example.com' format"; 
                    }
                    
                    $emailErrFlag = false;
                }
  
                if (empty($_POST["address"])) {
                    $address = "";
                } 
                else {
                    $address = test_input($_POST["address"]);
                }
            } 

            if( $nameErrFlag == false AND $phoneErrFlag == false AND $emailErrFlag == false)
            {
                $errFlag = false;
            }

            function test_input($data) {
                $data = trim($data);
                $data = stripslashes($data);
                $data = htmlspecialchars($data);
                return $data;
            }
        ?>
   
        <form id="myForm" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">

            <table style='border: solid 3px #f44336;'>

                <tr><th>Field Name</th><th>User Input</th></tr>

                <tr>
                    <td class='label'> First Name <span class=<?php if ($nameErrFlag=="true") {echo "error";} else {echo "noerror";}?>>*</span> </td>
                    <td><input type="text" name="fname" value="<?php echo $fname;?>"></td>
                </tr>

                <tr>
                    <td class='label'> Last Name <span class=<?php if ($nameErrFlag=="true") {echo "error";} else {echo "noerror";}?>>*</span> </td>
                    <td><input type="text" name="lname" value="<?php echo $lname;?>"></td>
                </tr>

                <tr>
                    <td class='label'> Phone Number <span class=<?php if ($phoneErrFlag=="true") {echo "error";} else {echo "noerror";}?>>*</span> </td>
                    <td><input type ="tel" name="phone" value="<?php echo $phone;?>"></td>
                </tr>

                <tr>
                    <td class='label'> Email <span class=<?php if ($emailErrFlag=="true") {echo "error";} else {echo "noerror";}?>>*</span> </td>
                    <td><input type ="text" name="email" value="<?php echo $email;?>"></td>
                </tr>
    
                <tr>
                    <td class='label'> Address  </td>
                    <td><textarea name="address" rows="5" cols="40"><?php echo $address;?></textarea></td>
                </tr>

                <tr style='text-align:right;'>
                    <td><b> * Required Field</b></td>
                    <td id="buttons">
                        <input type="submit" name="submit" value="Submit">
                        <input type="reset" name="reset" value="Reset">
                    </td>
                </tr> 
        </table>     
      </form>
    </div>

    <div class="w3-third w3-center">
        <h1>Feedback</h1>
        <h5>
            <?php

                if ($errFlag == true) {
                    echo "<span class='error'>";   // set the color red
                    echo "<ul>";

                    if (strlen($fnameErr) > 0) {
                        echo "<li> $fnameErr </li>";
                    }

                    if (strlen($lnameErr) > 0) {
                        echo "<li> $lnameErr </li>";
                    }

                    if (strlen($phoneErr) > 0) {
                        echo "<li> $phoneErr </li>";
                    }

                    if (strlen($emailErr) > 0) {
                        echo "<li> $emailErr </li>";
                    }

                    echo "</ul>";
                    echo "</span>";
                }

                else {
                    try {
                        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
                        // set the PDO error mode to exception
                        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                        $sql = "INSERT INTO tblUsers (fname, lname, phone, email, address)
                        VALUES ('$fname', '$lname', '$phone', '$email', '$address')";
                        // use exec() because no results are returned
                        $conn->exec($sql);
                        echo "New record created successfully!";
                    }
                    catch(PDOException $e) {
                        echo $sql . "<br>" . $e->getMessage();
                    }
                    $conn = null;
                }
            ?>
        </h5> 
        <i class="fa fa-user-plus w3-padding-64 w3-text-red"></i>
    </div>
  </div>
</div>


<!-- Footer -->
<footer class="w3-container w3-black w3-center w3-opacity w3-padding-8">
 <p>Powered by <a href="http://www.w3schools.com/w3css/default.asp" target="_blank">w3.css</a></p>
</footer>

<script>
    // Used to toggle the menu on small screens when clicking on the menu button
    function myFunction() {
        var x = document.getElementById("navDemo");
        if (x.className.indexOf("w3-show") == -1) {
            x.className += " w3-show";
        } else {
            x.className = x.className.replace(" w3-show", "");
        }
    }
</script>

</body>
</html>

