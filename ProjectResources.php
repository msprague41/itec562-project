<!DOCTYPE html>
<html>

<title>ITEC562-001 Project</title>

<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">

<style>
    body,h1,h2,h3,h4,h5,h6 {font-family: "Lato", sans-serif}
    
    .w3-navbar,h1,button {font-family: "Montserrat", sans-serif}
    .fa-pencil {font-size:200px}
</style>

<body>
<!-- Navbar -->
<div class="w3-top">
  <ul class="w3-navbar w3-red w3-card-2 w3-left-align w3-large">
    <li class="w3-hide-medium w3-hide-large w3-opennav w3-right">
      <a class="w3-padding-large w3-hover-white w3-large w3-red" href="javascript:void(0);" onclick="myFunction()" title="Toggle Navigation Menu"><i class="fa fa-bars"></i></a>
    </li>
    
    <li><a href="ProjectIndex.php" class="w3-padding-large w3-white">Home</a></li>
    
    <li class="w3-hide-small"><a href="ProjectContact.php" class="w3-padding-large w3-hover-white">Create Account</a></li>
    
    <li class="w3-dropdown-hover">
		<a>Static Queries <i class="fa fa-caret-down"></i></a>
			<div class="w3-dropdown-content w3-white w3-card-4">
				<a href="ProjectStatic1.php">Query 1</a>
				<a href="ProjectStatic2.php">Query 2</a>
				<a href="ProjectMultipleTable.php">Multiple Table Query</a>
			</div>
	</li>
    
    <li class="w3-dropdown-hover">
		<a>Ad Hoc Queries <i class="fa fa-caret-down"></i></a>
			<div class="w3-dropdown-content w3-white w3-card-4">
				<a href="ProjectAdHocSelectForm.php">Select Query</a>
				<a href="ProjectAdHocUpdateForm.php">Update Query</a>
			</div>
	</li>
    
    <li class="w3-hide-small"><a href="ProjectResources.php" class="w3-padding-large w3-hover-white">Resources Used</a></li>
	
  </ul>

<!-- Navbar on small screens -->
  <div id="navDemo" class="w3-hide w3-hide-large w3-hide-medium">
    <ul class="w3-navbar w3-left-align w3-large w3-black">
      
      <li><a class="w3-padding-large" href="ProjectContact.php">Create Account</a></li>
      
	  <li class="w3-dropdown-hover">
		<a>Static Queries <i class="fa fa-caret-down"></i></a>
			<div class="w3-dropdown-content w3-white w3-card-4">
				<a href="ProjectStatic1.php">Query 1</a>
				<a href="ProjectStatic2.php">Query 2</a>
				<a href="ProjectMultipleTable.php">Multiple Table Query</a>
			</div>
      </li>
      
      <li class="w3-dropdown-hover">
		<a>Ad Hoc Queries <i class="fa fa-caret-down"></i></a>
			<div class="w3-dropdown-content w3-white w3-card-4">
				<a href="ProjectAdHocSelectForm.php">Select Query</a>
				<a href="ProjectAdHocUpdateForm.php">Update Query</a>
			</div>
	  </li>
      
      <li><a class="w3-padding-large" href="ProjectResources.php">Resources Used</a></li>
    </ul>
  </div>
</div>

<!-- Header -->
<header class="w3-container w3-red w3-center w3-padding-128">
  <h1 class="w3-margin w3-jumbo">Resources Used</h1>
  <p class="w3-xlarge">This project couldn't have been possible without the input of the following sources</p>
</header>

<!-- First Grid -->
<div class="w3-row-padding w3-padding-64 w3-container">
  <div class="w3-content">
    <div class="w3-twothird">
      <h1>Works Cited</h1>
      <h5 class="w3-padding-32">
          For code validation: <a href="https://validator.w3.org/">W3C's Markup Validation Service</a>
          <br><br>
          For accessibility validation: <a href="http://hiis.isti.cnr.it:8080/MauveWeb/">MAUVE</a>
          <br><br>
          For the CSS template: <a href="http://www.w3schools.com/w3css/w3css_templates.asp">W3 Schools'  W3.CSS Templates</a>
          <br><br>
          For the database: <a href="http://mysql-tools.com/en/downloads/mysql-databases/4-sakila-db.html">MYSQL-Tools</a>
          <br><br>
          For HTML and SQL coding help: <a href="http://w3schools.com">W3 Schools</a>
          <br><br>
          For helpful editing and advice: Andrew Fuhs and Shelby Rushe
        </h5>
    </div>

    <div class="w3-third w3-center">
      <i class="fa fa-pencil w3-padding-64 w3-text-red"></i>
    </div>
  </div>
</div>

<!-- Footer -->
<footer class="w3-container w3-black w3-center w3-opacity w3-padding-8">
    <p>Powered by <a href="http://www.w3schools.com/w3css/default.asp" target="_blank">w3.css</a></p>
</footer>

<script>
    // Used to toggle the menu on small screens when clicking on the menu button
    function myFunction() {
        var x = document.getElementById("navDemo");
        if (x.className.indexOf("w3-show") == -1) {
            x.className += " w3-show";
        } else {
            x.className = x.className.replace(" w3-show", "");
        }
    }
</script>

</body>
</html>

